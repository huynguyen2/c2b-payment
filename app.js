const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const PORT = process.env.PORT || 8000;
const app = express();

app.use(cors());

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/callback', (req, res) => {
  console.log('callback!!!!');
  console.log('request:', req.body);
    
  res.status(200).json({
    message: 'success'
  });
});

app.listen(PORT, console.log(`App running, listen port ${PORT}`));